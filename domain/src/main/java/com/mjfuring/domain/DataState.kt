package com.mjfuring.domain

open class DataState {

    data class Success(val any: Any?) : DataState()

    data class Failed(val msg: String) : DataState()

}
