package com.mjfuring.domain

data class Profile(
    var id: Long,
    var givenName: String,
    var surname: String,
    var location: String,
    var about: String,
    var avatarUrl: String,
    var featuredFriends: List<Friend>,
    var shots: List<Shot>
)
