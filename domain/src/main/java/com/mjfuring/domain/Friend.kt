package com.mjfuring.domain

open class Friend(
    var id: Long = 0,
    var givenName: String = "",
    var surname: String = "",
    var avatarUrl: String = "",
    var isFavorite: Boolean = false
)
