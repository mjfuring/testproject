package com.mjfuring.domain

data class Shot(
    var id: Long,
    var imageUrl: String
)
