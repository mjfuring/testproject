package com.mjfuring.data

import com.mjfuring.domain.DataState
import kotlinx.coroutines.delay
import timber.log.Timber

interface IFriendRepo {
    suspend fun get(): DataState
}

class FriendRepo(
    private val apiService: ApiService
): IFriendRepo {

    override suspend fun get(): DataState {
        try {
            delay(1000)
            apiService.friends().apply {
                if (isSuccessful)
                    return DataState.Success(body()!!)
            }
        } catch (ex: Exception) {
            Timber.e(ex)
        }
        return DataState.Failed("")
    }

}
