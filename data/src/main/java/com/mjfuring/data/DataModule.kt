package com.mjfuring.data

import com.mjfuring.domain.Constant

import org.koin.dsl.module

@SuppressWarnings
val dataModule = module {

    single { provideHttp() }
    single { provideApi<ApiService>(get(), Constant.BASE_URL) }

    factory { ProfileRepo(get()) as IProfileRepo }
    factory { FriendRepo(get())  as IFriendRepo }

}





