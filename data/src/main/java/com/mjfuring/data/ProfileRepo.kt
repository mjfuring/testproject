package com.mjfuring.data

import com.mjfuring.domain.DataState
import kotlinx.coroutines.delay
import timber.log.Timber

interface IProfileRepo {
    suspend fun get(): DataState
}

class ProfileRepo(
    private val apiService: ApiService
): IProfileRepo {

    override suspend fun get(): DataState {
        try {
            delay(1000)
            apiService.profile().apply {
                if (isSuccessful)
                    return DataState.Success(body()!!)
            }
        } catch (ex: Exception) {
            Timber.e(ex)
        }
        return DataState.Failed("")
    }

}
