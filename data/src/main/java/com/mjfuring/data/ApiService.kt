package com.mjfuring.data

import com.mjfuring.domain.Friend
import com.mjfuring.domain.Profile
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("profile")
    suspend fun profile(): Response<Profile>

    @GET("friends")
    suspend fun friends(): Response<List<Friend>>

}