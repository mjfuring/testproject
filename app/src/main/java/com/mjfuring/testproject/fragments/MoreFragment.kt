package com.mjfuring.testproject.fragments

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mjfuring.testproject.R
import com.mjfuring.testproject.databinding.FragmentMoreBinding

class MoreFragment: BottomSheetDialogFragment() {

    private var viewBinding: FragmentMoreBinding? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener {
                val bottomSheet = findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                bottomSheet.setBackgroundResource(android.R.color.transparent)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_more, container, false)
        viewBinding?.apply {
            tvCancel.setOnClickListener { dismiss() }
            tvAbout.setOnClickListener {
                dismiss()
                findNavController().navigate(
                    ProfileFragmentDirections.actionProfileFragmentToAboutFragment()
                )
            }
        }
        return viewBinding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewBinding = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewBinding?.apply {


        }
    }


}