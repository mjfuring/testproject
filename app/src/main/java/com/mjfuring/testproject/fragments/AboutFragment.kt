package com.mjfuring.testproject.fragments

import androidx.navigation.fragment.findNavController
import com.mjfuring.testproject.R
import com.mjfuring.testproject.common.BaseFragment
import com.mjfuring.testproject.databinding.FragmentAboutBinding

class AboutFragment: BaseFragment<FragmentAboutBinding>() {

    override fun layoutRes(): Int = R.layout.fragment_about

    override fun onInit(viewBinding: FragmentAboutBinding) {
        setHasOptionsMenu(true)
        viewBinding.apply {
            toolbar.apply {
                setOnMenuItemClickListener {
                    onMenuClick(it.itemId)
                    true
                }
            }
            context?.apply {
                val info = packageManager.getPackageInfo(packageName, 0)
                tvVersion.text = getString(R.string.about_version, info.versionName)
            }
        }
    }

    private fun onMenuClick(id: Int){
        when(id){
            R.id.action_close -> {
               findNavController().popBackStack()
            }
        }
    }

}