package com.mjfuring.testproject.fragments

import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mjfuring.domain.DataState
import com.mjfuring.domain.Friend
import com.mjfuring.testproject.R
import com.mjfuring.testproject.adapters.FriendAdapterVertical
import com.mjfuring.testproject.common.BaseFragment
import com.mjfuring.testproject.databinding.FragmentFriendBinding
import com.mjfuring.testproject.viewmodels.FriendViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class FriendFragment: BaseFragment<FragmentFriendBinding>() {

    private val viewModel: FriendViewModel by viewModel()
    private val friendAdapter = FriendAdapterVertical()

    override fun layoutRes(): Int = R.layout.fragment_friend

    override fun onInit(viewBinding: FragmentFriendBinding) {
        viewBinding.apply {
            toolbar.setNavigationOnClickListener {
                findNavController().popBackStack()
            }
            rvFriends.apply {
                hasFixedSize()
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = friendAdapter
            }
        }
        viewModel.apply {
            liveData.observe(this@FriendFragment, {
                when (it) {
                    is DataState.Success -> {
                        showShimmer(false)
                        val any = it.any
                        if (any is List<*>){
                            friendAdapter.insertAll(any.filterIsInstance<Friend>())
                            viewBinding.rvFriends.scheduleLayoutAnimation()
                        }
                    }
                    is DataState.Failed -> {

                    }
                }
            })
            showShimmer()
            getFriends()
        }
    }

    private fun showShimmer(show: Boolean = true){
        viewBinding?.apply {
            if (show){
                shimmer.startShimmer()
                shimmer.isVisible = true
                rvFriends.isVisible = false
            } else {
                shimmer.stopShimmer()
                shimmer.isVisible = false
                rvFriends.isVisible = true
            }
        }
    }
}