package com.mjfuring.testproject.fragments

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.mjfuring.domain.DataState
import com.mjfuring.domain.Profile
import com.mjfuring.domain.Shot
import com.mjfuring.testproject.R
import com.mjfuring.testproject.adapters.FriendAdapterHorizontal
import com.mjfuring.testproject.adapters.ShotAdapter
import com.mjfuring.testproject.common.BaseFragment
import com.mjfuring.testproject.databinding.FragmentProfileBinding
import com.mjfuring.testproject.viewmodels.ProfileViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class ProfileFragment: BaseFragment<FragmentProfileBinding>() {

    private val viewModel: ProfileViewModel by viewModel()
    private val friendAdapter = FriendAdapterHorizontal()
    private val shotAdapter = ShotAdapter()

    override fun layoutRes(): Int = R.layout.fragment_profile

    override fun onInit(viewBinding: FragmentProfileBinding) {
        viewBinding.apply {
            toolbar.apply {
                setOnMenuItemClickListener {
                    onMenuClick(it.itemId)
                    true
                }
            }
            incProfileBody.rvFriends.apply {
                hasFixedSize()
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = friendAdapter
            }
            incProfileBody.rvShots.apply {
                hasFixedSize()
                layoutManager = GridLayoutManager(requireContext(), 3)
                adapter = shotAdapter
            }
            incProfileBody.tvSeeAll.setOnClickListener {
                findNavController().navigate(
                    ProfileFragmentDirections.actionProfileFragmentToFriendFragment()
                )
            }
        }
        viewModel.apply {
            liveData.observe(this@ProfileFragment, {
                when (it) {
                    is DataState.Success -> {
                        showShimmer(false)
                        showData(it.any as Profile)
                    }
                    is DataState.Failed -> {

                    }
                }
            })
            getProfile()
            showShimmer(true)
        }
    }

    private fun showData(profile: Profile){
        viewBinding?.apply {
            Glide.with(this@ProfileFragment)
                .load(profile.avatarUrl)
                .circleCrop()
                .into(incProfile.profileImage)
            incProfile.profileName.text = getString(R.string.full_name, profile.givenName, profile.surname)
            incProfile.profileLocation.text = profile.location
            if (friendAdapter.items.isNullOrEmpty()){
                friendAdapter.insertAll(profile.featuredFriends)
                incProfileBody.rvFriends.scheduleLayoutAnimation()
                shotAdapter.insertAll(profile.shots)
            }
        }
    }

    private fun onMenuClick(id: Int){
        when(id){
            R.id.action_more -> {
                MoreFragment().show(childFragmentManager, "More Fragment")
            }
        }
    }

    private fun showShimmer(show: Boolean = true){
        viewBinding?.apply {
            if (show){
                shimmer.startShimmer()
                shimmer.isVisible = true
                (incProfileBody.root as ConstraintLayout).isVisible = false
            } else {
                shimmer.stopShimmer()
                shimmer.isVisible = false
                (incProfileBody.root as ConstraintLayout).isVisible = true
            }
        }
    }

}