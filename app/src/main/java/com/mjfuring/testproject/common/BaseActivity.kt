package com.mjfuring.testproject.common

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding


abstract class BaseActivity<B : ViewDataBinding>: AppCompatActivity() {

    private lateinit var viewBinding: B

    @LayoutRes
    abstract fun layoutRes(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this, layoutRes())
        setContentView(viewBinding.root)
        onInit(viewBinding)
    }

    open fun onInit(viewBinding: B) {}

}