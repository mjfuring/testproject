package com.mjfuring.testproject.common

import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T> : RecyclerView.Adapter<BaseViewHolder<T>>() {

    val items = ArrayList<T>()

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(baseViewHolder: BaseViewHolder<T>, i: Int) {
        baseViewHolder.bind(items[i])
    }

}