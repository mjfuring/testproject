package com.mjfuring.testproject.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment


abstract class BaseFragment<B : ViewDataBinding>: Fragment() {

    var viewBinding: B? = null

    @LayoutRes
    abstract fun layoutRes(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        viewBinding = DataBindingUtil.inflate(inflater, layoutRes(), container, false)
        return viewBinding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onInit(viewBinding!!)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewBinding = null
    }

    open fun onInit(viewBinding: B) {}

}