package com.mjfuring.testproject.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mjfuring.domain.Friend
import com.mjfuring.domain.Shot
import com.mjfuring.testproject.R
import com.mjfuring.testproject.common.BaseAdapter
import com.mjfuring.testproject.common.BaseViewHolder
import com.mjfuring.testproject.databinding.ItemFriendVerticalBinding
import com.mjfuring.testproject.databinding.ItemShotBinding

class ShotAdapter(
    private val onClick: ((item: Any) -> Unit)? = null
): BaseAdapter<Shot>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Shot> {

        val dataBinding: ItemShotBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_shot,
            parent,
            false)

        return InnerViewHolder(dataBinding).apply {
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onClick?.invoke(items[adapterPosition])
                }
            }
        }
    }

    inner class InnerViewHolder(
        private val binding: ItemShotBinding
    ) : BaseViewHolder<Shot>(binding.root) {
        override fun bind(item: Shot) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(item.imageUrl)
                    .centerCrop()
                    .into(ivIcon)
            }

        }
    }

    fun insertAll(shots: List<Shot>){
        items.addAll(shots)
        notifyDataSetChanged()
    }

}