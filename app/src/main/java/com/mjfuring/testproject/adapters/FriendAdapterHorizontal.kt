package com.mjfuring.testproject.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mjfuring.domain.Friend
import com.mjfuring.testproject.R
import com.mjfuring.testproject.common.BaseAdapter
import com.mjfuring.testproject.common.BaseViewHolder
import com.mjfuring.testproject.databinding.ItemFriendHorizontalBinding


class FriendAdapterHorizontal(
    private val onClick: ((item: Any) -> Unit)? = null
): BaseAdapter<Friend>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Friend> {

        val dataBinding: ItemFriendHorizontalBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_friend_horizontal,
            parent,
            false)

        return InnerViewHolder(dataBinding).apply {
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onClick?.invoke(items[adapterPosition])
                }
            }
        }
    }

    inner class InnerViewHolder(
        private val binding: ItemFriendHorizontalBinding
    ) : BaseViewHolder<Friend>(binding.root) {
        override fun bind(item: Friend) {
            binding.apply {
                tvName.text = item.givenName
                if (item.isFavorite){
                    ivIcon.setBackgroundResource(R.drawable.circle)
                } else {
                    ivIcon.setBackgroundResource(0)
                }
                Glide.with(itemView.context)
                    .load(item.avatarUrl)
                    .circleCrop()
                    .into(ivIcon)
            }

        }
    }

    fun insertAll(friends: List<Friend>){
        items.addAll(friends)
        notifyDataSetChanged()
    }

}