package com.mjfuring.testproject.adapters

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mjfuring.domain.Friend
import com.mjfuring.testproject.R
import com.mjfuring.testproject.common.BaseAdapter
import com.mjfuring.testproject.common.BaseViewHolder
import com.mjfuring.testproject.databinding.ItemFriendVerticalBinding

class FriendAdapterVertical(
    private val onClick: ((item: Any) -> Unit)? = null
): BaseAdapter<Friend>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Friend> {

        val dataBinding: ItemFriendVerticalBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_friend_vertical,
            parent,
            false)

        return InnerViewHolder(dataBinding).apply {
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onClick?.invoke(items[adapterPosition])
                }
            }
        }
    }

    inner class InnerViewHolder(
        private val binding: ItemFriendVerticalBinding
    ) : BaseViewHolder<Friend>(binding.root) {
        override fun bind(item: Friend) {
            binding.apply {
                if(item.isFavorite){
                    tvFavorite.isVisible = true
                    tvName.typeface = Typeface.create(tvName.typeface, Typeface.BOLD)
                    ivIcon.setBackgroundResource(R.drawable.circle)
                } else {
                    tvFavorite.isVisible = false
                    tvName.typeface = Typeface.create(tvName.typeface, Typeface.NORMAL)
                    ivIcon.setBackgroundResource(0)
                }
                tvName.text = itemView.context.getString(R.string.full_name, item.givenName, item.surname)
                Glide.with(itemView.context)
                    .load(item.avatarUrl)
                    .circleCrop()
                    .into(ivIcon)
            }

        }
    }

    fun insertAll(friends: List<Friend>){
        items.addAll(friends)
        notifyDataSetChanged()
    }

}