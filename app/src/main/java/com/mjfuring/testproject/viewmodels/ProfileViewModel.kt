package com.mjfuring.testproject.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mjfuring.data.IProfileRepo
import com.mjfuring.domain.DataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProfileViewModel(
    private var profileRepo: IProfileRepo
): ViewModel() {

    val liveData = MutableLiveData<DataState>()

    fun getProfile() {
        viewModelScope.launch(Dispatchers.IO) {
            liveData.postValue(profileRepo.get())
        }
    }

}