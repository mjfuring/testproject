package com.mjfuring.testproject.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mjfuring.data.IFriendRepo
import com.mjfuring.domain.DataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FriendViewModel(
    private var friendRepo: IFriendRepo
): ViewModel() {

    val liveData = MutableLiveData<DataState>()

    fun getFriends() {
        viewModelScope.launch(Dispatchers.IO) {
            liveData.postValue(friendRepo.get())
        }
    }

}