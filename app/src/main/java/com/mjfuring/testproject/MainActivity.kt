package com.mjfuring.testproject

import com.mjfuring.testproject.common.BaseActivity
import com.mjfuring.testproject.databinding.ActivityMainBinding

class MainActivity: BaseActivity<ActivityMainBinding>() {

    override fun layoutRes() = R.layout.activity_main

    override fun onInit(viewBinding: ActivityMainBinding) {

    }

}