package com.mjfuring.testproject.injection

import com.mjfuring.testproject.viewmodels.FriendViewModel
import com.mjfuring.testproject.viewmodels.ProfileViewModel
import org.koin.androidx.viewmodel.dsl.viewModel

import org.koin.dsl.module

val appModule = module {

    viewModel { ProfileViewModel(get()) }
    viewModel { FriendViewModel(get()) }

}





