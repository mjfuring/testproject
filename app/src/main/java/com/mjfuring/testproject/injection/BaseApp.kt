package com.mjfuring.testproject.injection

import android.app.Application
import com.mjfuring.data.dataModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.core.context.startKoin

class BaseApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@BaseApp)
            modules(appModule, dataModule)
            androidFileProperties()
        }
    }

}